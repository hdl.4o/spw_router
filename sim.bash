#!/bin/bash

cd modelsim
rm -rf work
touch wave.do

if [ "$1" == "-i" ]; then
    declare -i SECONDS
    declare -i min
    declare -i min_sec
    declare -i sec

    SECONDS=0
    modelsim -do sim.fdo

    min=$SECONDS/60
    min_sec=$min*60
    sec=$SECONDS-$min_sec
    echo "done in $min minutes and $sec seconds"
else
    declare -i SECONDS
    declare -i min
    declare -i min_sec
    declare -i sec

    SECONDS=0
    vsim -c -immedassert -do sim.fdo -hazards

    min=$SECONDS/60
    min_sec=$min*60
    sec=$SECONDS-$min_sec
    echo "done in $min minutes and $sec seconds"
fi
