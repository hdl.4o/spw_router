-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;


entity tx_parse_addr is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iValid: in std_logic;
        oRd: out std_logic;
        iSpw_data: in std_logic_vector (8 downto 0);

        oId_valid: out std_logic;
        oId: out std_logic_vector (7 downto 0);

        oValid: out std_logic;
        iRd: in std_logic_vector;
        oSpw_data: out std_logic_vector (8 downto 0)
    );
end entity;

architecture v1 of tx_parse_addr is

    component tx_parse_addr_eop_detector
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iRd: in std_logic_vector;
            iSpw_data: in std_logic_vector (8 downto 0);

            oEop: out std_logic
        );
    end component;

    component data_demux
        port (
            iValid: in std_logic;
            oRd: out std_logic;
            iSpw_data: in std_logic_vector (8 downto 0);

            oValid: out std_logic;
            iRd: in std_logic_vector;
            oSpw_data: out std_logic_vector (8 downto 0)
        );
    end component;

    component tx_parse_addr_read_addr
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iEop: in std_logic;

            iValid: in std_logic;
            oRd: out std_logic;
            iSpw_data: in std_logic_vector (8 downto 0);

            oId_valid: out std_logic;
            oId: out std_logic_vector (7 downto 0)
        );
    end component;

    signal sEop: std_logic;
    signal sValid: std_logic;
    signal sRd: std_logic;
    signal sData: std_logic_vector (8 downto 0);

    signal sRd_full: std_logic_vector (iRd'length downto 0);

begin

    oValid <= sValid;
    oSpw_data <= sData;

    sRd_full (iRd'length-1 downto 0) <= iRd;
    sRd_full (iRd'length) <= sRd;

    eop_detector: tx_parse_addr_eop_detector
        port map (
            iClk => iClk,
            iReset => iReset,

            iRd => iRd,
            iSpw_data => sData,

            oEop => sEop
        );

    mux: data_demux
        port map (
            iValid => iValid,
            oRd => oRd,
            iSpw_data => iSpw_data,

            oValid => sValid,
            iRd => sRd_full,
            oSpw_data => sData
        );

    read_addr: tx_parse_addr_read_addr
        port map (
            iClk => iClk,
            iReset => iReset,

            iEop => sEop,

            iValid => sValid,
            oRd => sRd,
            iSpw_data => sData,

            oId_valid => oId_valid,
            oId => oId
        );

end v1;
