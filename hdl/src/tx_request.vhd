-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.spw_router_pkg.all;


entity tx_request is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iId_valid: in std_logic;
        iId: in std_logic_vector (7 downto 0);
        iIds: in tAddr_ar;

        oRequest: out std_logic_vector
    );
end entity;

architecture v1 of tx_request is

    constant cN: natural := iIds'length;

    component tx_request_cmp
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iId_valid: in std_logic;
            iId: in std_logic_vector (7 downto 0);
            iIds: in tAddr_ar;

            oMatch: out std_logic_vector
        );
    end component;

    component priority_selector
        port (
            iData: in std_logic_vector;
            oData: out std_logic_vector
        );
    end component;

    component reg
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iData: in std_logic_vector;
            oData: out std_logic_vector
        );
    end component;

    signal sMatch: std_logic_vector (cN-1 downto 0);
    signal sRequest: std_logic_vector (cN-1 downto 0);

begin

    assert oRequest'length = cN
        report "width error"
        severity failure;

    cmp: tx_request_cmp
        port map (
            iClk => iClk,
            iReset => iReset,

            iId_valid => iId_valid,
            iId => iId,
            iIds => iIds,

            oMatch => sMatch
        );

    pick_one: priority_selector
        port map (
            iData => sMatch,
            oData => sRequest
        );

    request_reg: reg
        port map (
            iClk => iClk,
            iReset => iReset,

            iData => sRequest,
            oData => oRequest
        );

end v1;
