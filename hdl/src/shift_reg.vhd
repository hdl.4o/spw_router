-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;


entity shift_reg is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iShift: in std_logic;

        oVector: out std_logic_vector
    );
end entity;

architecture v1 of shift_reg is

    constant cN: natural := oVector'length;

    component reg_en
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iEn: in std_logic;
            iData: in std_logic_vector;

            oData: out std_logic_vector
        );
    end component;

    signal sVector: std_logic_vector (cN-1 downto 0);
    signal sVector_reg: std_logic_vector (cN-1 downto 0);

begin

    oVector <= sVector_reg (cN-1 downto 1) & not sVector_reg (0);

    process (sVector_reg)
    begin
        sVector (0) <= not sVector_reg (cN-1);
        sVector (1) <= not sVector_reg (0);
    end process;

    long_enough: if cN > 2 generate

        process (sVector_reg)
        begin
            sVector (cN-1 downto 2) <= sVector_reg (cN-2 downto 1);
        end process;

    end generate;

    mask_reg: reg_en
        port map (
            iClk => iClk,
            iReset => iReset,

            iEn => iShift,
            iData => sVector,

            oData => sVector_reg
        );

end v1;
