-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.spw_router_pkg.all;


entity rx_mux is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iStart: in std_logic;
        iMask: in std_logic_vector;

        iValid: in std_logic_vector;
        oRd: out std_logic_vector;
        iSpw_data: in tSpw_data_ar;

        oValid: out std_logic;
        iRd: in std_logic;
        oSpw_data: out std_logic_vector (8 downto 0)
    );
end entity;

architecture v1 of rx_mux is

    constant cUSE_MODEL: boolean := true;

begin

    model: if cUSE_MODEL generate

        function mux (
            iSpw_data: tSpw_data_ar;
            iMask: std_logic_vector
        ) return std_logic_vector is
            variable still_looping: boolean := true;
            variable ret: std_logic_vector (8 downto 0);
        begin
            for i in iMask'range loop
                if iMask (i) = '1' then
                    assert still_looping
                        report "mux() error: not a onehot mask"
                        severity failure;

                    ret := iSpw_data (i);
                    still_looping := false;
                end if;
            end loop;

            return ret;
        end function;

        constant cN: natural := iMask'length;
        signal sEnable: std_logic;
        signal sData: std_logic_vector (8 downto 0);
    begin

        oSpw_data <= sData;

        process
            constant gReset_active_lvl: std_logic := '0';
            variable do_init: boolean := true;
        begin
            if do_init then
                sEnable <= '0';
                if iReset /= gReset_active_lvl then
                    wait until iReset = gReset_active_lvl;
                end if;
                wait until iReset = not gReset_active_lvl;
                do_init := false;
            end if;

            catch_start: while true loop
                wait until iClk'event and iClk = '1';
                if iStart = '1' then
                    exit catch_start;
                end if;
            end loop;

            sEnable <= '1';

            catch_eop: while true loop
                wait until iClk'event and iClk = '1';
                if iRd = '1' then
                    if sData (8) = '1' then
                        exit catch_eop;
                    end if;
                end if;
            end loop;

            sEnable <= '0';

        end process;

        oValid <= '1'
            when (iValid and iMask) /= (iValid'range => '0') and sEnable = '1'
            else '0';

        rd_bits: for i in oRd'range generate
            oRd (i) <= '1' when iRd = '1' and iMask (i) = '1' and sEnable = '1'
                else '0';
        end generate;

        sData <= mux (iSpw_data, iMask);

    end generate;

    combat_code: if not cUSE_MODEL generate

        assert false
            report "implement me"
            severity failure;

    end generate;

end v1;
