-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.spw_router_pkg.all;


entity rx_mux_logic is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iEnable: in std_logic;
        iMask: in std_logic_vector;

        iValid: in std_logic_vector;
        oRd: out std_logic_vector;
        iSpw_data: in tSpw_data_ar;

        oValid: out std_logic;
        iRd: in std_logic;
        oSpw_data: out std_logic_vector (8 downto 0)
    );
end entity;

architecture v1 of rx_mux_logic is

    constant cN: natural := iValid'length;

    type tData_ar_inv is array (8 downto 0) of std_logic_vector (cN-1 downto 0);

    signal sValid: std_logic_vector (iValid'range);
    signal sData_inv: tData_ar_inv;

begin

    assert iMask'length = cN
        report "width error (iMask)"
        severity failure;

    assert iMask'low = 0
        report "range error (iMask)"
        severity failure;

    assert iValid'length = cN
        report "width error (iValid)"
        severity failure;

    assert iValid'low = 0
        report "range error (iValid)"
        severity failure;

    assert oRd'length = cN
        report "width error (oRd)"
        severity failure;

    assert oRd'low = 0
        report "range error (oRd)"
        severity failure;

    assert iSpw_data'length = cN
        report "width error (iSpw_data)"
        severity failure;

    assert iSpw_data'low = 0
        report "range error (iSpw_data)"
        severity failure;

    valid_bits: for i in iValid'range generate
        sValid (i) <= iValid (i) and iMask (i);
    end generate;

    oValid <= '0' when sValid = (sValid'range => '0') else '1';


    data_bits_x: for i in iSpw_data'range generate

        data_bits_y: for j in 8 downto 0 generate
            sData_inv (j) (i) <= iSpw_data (i) (j) when iMask (i) = '1'
                else '0';
        end generate;

    end generate;

    odata_bits: for i in oSpw_data'range generate
        oSpw_data (i) <= '0' when sData_inv (i) = (8 downto 0 => '0') else '1';
    end generate;

    rd_bits: for i in oRd'range generate
        oRd (i) <= iRd and iMask (i);
    end generate;

end v1;
