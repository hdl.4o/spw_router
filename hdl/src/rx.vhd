-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.spw_router_pkg.all;


entity rx is
    generic (
        gId: natural
    );
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        oId: out std_logic_vector (7 downto 0);

        iRequest: in std_logic_vector;
        oAvailable: out std_logic_vector;
        iBind: in std_logic_vector;

        iValid: in std_logic_vector;
        oRd: out std_logic_vector;
        iSpw_data: in tSpw_data_ar;

        oValid: out std_logic;
        iRd: in std_logic;
        oSpw_data: out std_logic_vector (8 downto 0)
    );
end entity;

architecture v1 of rx is

    constant cN: natural := iRequest'length;

    component rx_bind
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iRequest: in std_logic_vector;
            oAvailable: out std_logic_vector;
            iBind: in std_logic_vector;

            oStart: out std_logic;
            oMask: out std_logic_vector
        );
    end component;

    component rx_mux
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iStart: in std_logic;
            iMask: in std_logic_vector;

            iValid: in std_logic_vector;
            oRd: out std_logic_vector;
            iSpw_data: in tSpw_data_ar;

            oValid: out std_logic;
            iRd: in std_logic;
            oSpw_data: out std_logic_vector (8 downto 0)
        );
    end component;

    signal sStart: std_logic;
    signal sMask: std_logic_vector (cN-1 downto 0);

begin

    assert gId < 256
        report "invalid id"
        severity failure;

    assert oAvailable'length = cN
        report "width error (iAvailable)"
        severity failure;

    assert iBind'length = cN
        report "width error (oBind)"
        severity failure;

    assert iValid'length = cN
        report "width error (iValid)"
        severity failure;

    assert oRd'length = cN
        report "width error (oRd)"
        severity failure;

    assert iSpw_data'length = cN
        report "width error (iSpw_data)"
        severity failure;

    oId <= std_logic_vector (to_unsigned (gId+1, 8));

    bind: rx_bind
        port map (
            iClk => iClk,
            iReset => iReset,

            iRequest => iRequest,
            oAvailable => oAvailable,
            iBind => iBind,

            oStart => sStart,
            oMask => sMask
        );

    mux: rx_mux
        port map (
            iClk => iClk,
            iReset => iReset,

            iStart => sStart,
            iMask => sMask,

            iValid => iValid,
            oRd => oRd,
            iSpw_data => iSpw_data,

            oValid => oValid,
            iRd => iRd,
            oSpw_data => oSpw_data
        );

end v1;
