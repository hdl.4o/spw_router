-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;


entity rx_bind is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iRequest: in std_logic_vector;
        oAvailable: out std_logic_vector;
        iBind: in std_logic_vector;

        oStart: out std_logic;
        oMask: out std_logic_vector
    );
end entity;

architecture v1 of rx_bind is

    constant cUSE_MODEL: boolean := true;

    constant cN: natural := iRequest'length;

begin

    assert oAvailable'length = cN
        report "width error (oAvailable)"
        severity failure;

    assert oAvailable'low = 0
        report "range error (oAvailable)"
        severity failure;

    assert iBind'length = cN
        report "width error (iBind)"
        severity failure;

    assert iBind'low = 0
        report "range error (iBind)"
        severity failure;

    model: if cUSE_MODEL generate

        process

            procedure pump (i: natural) is
            begin
                wait until iClk'event and iClk = '0';
                oAvailable (i) <= '1';

                wait_bind: while true loop

                    wait until iClk'event and iClk = '1';
                    if iRequest (i) = '0' then
                        oAvailable (i) <= '0';
                        return;
                    end if;

                    if iBind (i) = '1' then
                        exit wait_bind;
                    end if;

                end loop;

                wait until iClk'event and iClk = '0';
                oMask (i) <= '1';
                oStart <= '1';
                wait until iClk'event and iClk = '0';
                oStart <= '0';

                wait_done: while true loop
                    wait until iClk'event and iClk = '1';
                    if iRequest (i) = '0' then
                        exit wait_done;
                    end if;
                end loop;
                oAvailable (i) <= '0';
                oMask (i) <= '0';
            end procedure;

            constant gReset_active_lvl: std_logic := '0';
            variable do_init: boolean := true;
        begin
            if do_init then
                oAvailable <= (oAvailable'range => '0');
                oStart <= '0';
                oMask <= (oMask'range => '0');
                if iReset /= gReset_active_lvl then
                    wait until iReset = gReset_active_lvl;
                end if;
                wait until iReset = not gReset_active_lvl;
                do_init := false;
            end if;

            for i in 0 to cN-1 loop

                wait until iClk'event and iClk = '0';
                if iRequest (i) = '1' then
                    pump (i);
                end if;
            end loop;

        end process;

    end generate;

    combat_code: if not cUSE_MODEL generate

        assert false
            report "implement me"
            severity failure;

    end generate;

end v1;
