-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


-- merge this OR to the one in cache block


library ieee;
use ieee.std_logic_1164.all;


entity tx_parse_addr_eop_detector is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iRd: in std_logic_vector;
        iSpw_data: in std_logic_vector (8 downto 0);

        oEop: out std_logic
    );
end entity;

architecture v1 of tx_parse_addr_eop_detector is

    component reg
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iData: in std_logic_vector;
            oData: out std_logic_vector
        );
    end component;

    signal sEop: std_logic;

begin

    sEop <= '1' when iRd /= (iRd'range => '0') and iSpw_data (8) = '1' else '0';

    eop_reg: reg
        port map (
            iClk => iClk,
            iReset => iReset,

            iData (0) => sEop,
            oData (0) => oEop
        );

end v1;
