-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.spw_router_pkg.all;


entity spw_router is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iPorts: in tSpw_halfport_ar;
        oPorts: out tSpw_halfport_ar
    );
end entity;

architecture v1 of spw_router is

    constant cN: natural := iPorts'length;

    type tAr_ar is array (cN-1 downto 0) of std_logic_vector (cN-1 downto 0);

    component tx
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            -- physical and virtual ports. w = N*M
            iIds: in tAddr_ar;
            -- physical, virtual and zero ports. w = N+1
            oRequest: out std_logic_vector;
            iAvailable: in std_logic_vector;
            oBind: out std_logic_vector;

            oValid: out std_logic;
            iRd: in std_logic_vector;
            oSpw_data: out std_logic_vector (8 downto 0);

            iValid: in std_logic;
            oRd: out std_logic;
            iSpw_data: in std_logic_vector (8 downto 0)
        );
    end component;

    component rx
        generic (
            gId: natural
        );
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            oId: out std_logic_vector (7 downto 0);

            iRequest: in std_logic_vector;
            oAvailable: out std_logic_vector;
            iBind: in std_logic_vector;

            iValid: in std_logic_vector;
            oRd: out std_logic_vector;
            iSpw_data: in tSpw_data_ar;

            oValid: out std_logic;
            iRd: in std_logic;
            oSpw_data: out std_logic_vector (8 downto 0)
        );
    end component;

    signal sIds: tAddr_ar (cN-1 downto 0);

    signal sRequest_tx: tAr_ar;
    signal sAvailable_tx: tAr_ar;
    signal sBind_tx: tAr_ar;
    signal sRd_tx: tAr_ar;

    signal sRequest_rx: tAr_ar;
    signal sAvailable_rx: tAr_ar;
    signal sBind_rx: tAr_ar;
    signal sRd_rx: tAr_ar;

    signal sValid: std_logic_vector (cN-1 downto 0);
    signal sSpw_data: tSpw_data_ar (cN-1 downto 0);

begin

    assert oPorts'length = cN
        report "width error"
        severity failure;

    assert iPorts'low = 0
        report "range error (iPorts)"
        severity failure;

    assert oPorts'low = 0
        report "range error (oPorts)"
        severity failure;

    ports: for i in cN-1 downto 0 generate

        port_out: tx
            port map (
                iClk => iClk,
                iReset => iReset,

                -- physical and virtual ports. w = N*M
                iIds => sIds,
                -- physical, virtual and zero ports. w = N+1
                oRequest => sRequest_tx (i),
                iAvailable => sAvailable_tx (i),
                oBind => sBind_tx (i),

                oValid => sValid (i),
                iRd => sRd_tx (i),
                oSpw_data => sSpw_data (i),

                iValid => iPorts(i).valid,
                oRd => oPorts(i).rd,
                iSpw_data => iPorts(i).data
            );

        bits: for j in cN-1 downto 0 generate

            sRequest_rx (i) (j) <= sRequest_tx (j) (i);
            sBind_rx (i) (j) <= sBind_tx (j) (i);

            sRd_tx (j) (i) <= sRd_rx (i) (j);
            sAvailable_tx (j) (i) <= sAvailable_rx (i) (j);

        end generate;

        port_in: rx
            generic map (
                gId => i
            )
            port map (
                iClk => iClk,
                iReset => iReset,

                oId => sIds (i),

                iRequest => sRequest_rx (i),
                oAvailable => sAvailable_rx (i),
                iBind => sBind_rx (i),

                iValid => sValid,
                oRd => sRd_rx (i),
                iSpw_data => sSpw_data,

                oValid => oPorts(i).valid,
                iRd => iPorts(i).rd,
                oSpw_data => oPorts(i).data
            );

    end generate;

end v1;
