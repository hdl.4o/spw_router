-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
library work;
use work.spw_router_pkg.all;


entity tx is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        -- physical and virtual ports. w = N*M
        iIds: in tAddr_ar;
        -- physical, virtual and zero ports. w = N+1
        oRequest: out std_logic_vector;
        iAvailable: in std_logic_vector;
        oBind: out std_logic_vector;

        oValid: out std_logic;
        iRd: in std_logic_vector;
        oSpw_data: out std_logic_vector (8 downto 0);

        iValid: in std_logic;
        oRd: out std_logic;
        iSpw_data: in std_logic_vector (8 downto 0)
    );
end entity;

architecture v1 of tx is

    component tx_parse_addr
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iValid: in std_logic;
            oRd: out std_logic;
            iSpw_data: in std_logic_vector (8 downto 0);

            oId_valid: out std_logic;
            oId: out std_logic_vector (7 downto 0);

            oValid: out std_logic;
            iRd: in std_logic_vector;
            oSpw_data: out std_logic_vector (8 downto 0)
        );
    end component;

    component tx_request
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iId_valid: in std_logic;
            iId: in std_logic_vector (7 downto 0);
            iIds: in tAddr_ar;

            oRequest: out std_logic_vector
        );
    end component;

    component tx_bind
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iId_valid: in std_logic;
            iAvailable: in std_logic_vector;
            oBind: out std_logic_vector
        );
    end component;

    signal sId_valid: std_logic;
    signal sId: std_logic_vector (7 downto 0);

begin

    parser: tx_parse_addr
        port map (
            iClk => iClk,
            iReset => iReset,

            iValid => iValid,
            oRd => oRd,
            iSpw_data => iSpw_data,

            oId_valid => sId_valid,
            oId => sId,

            oValid => oValid,
            iRd => iRd,
            oSpw_data => oSpw_data
        );

    request: tx_request
        port map (
            iClk => iClk,
            iReset => iReset,

            iId_valid => sId_valid,
            iId => sId,
            iIds => iIds,

            oRequest => oRequest
        );

    bind: tx_bind
        port map (
            iClk => iClk,
            iReset => iReset,

            iId_valid => sId_valid,
            iAvailable => iAvailable,
            oBind => oBind
        );

end v1;
