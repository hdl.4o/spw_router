library ieee;
use ieee.std_logic_1164.all;


package spw_router_pkg is

    subtype tAddr is std_logic_vector (7 downto 0);

    type tAddr_ar is array (natural range <>) of tAddr;

    type tSpw_data_ar is array (natural range <>)
        of std_logic_vector (8 downto 0);

    type tSpw_halfport is record
        valid: std_logic;
        rd: std_logic;
        data: std_logic_vector (8 downto 0);
    end record;

    type tSpw_halfport_ar is array (natural range <>) of tSpw_halfport;

end package;

package body spw_router_pkg is
end package body;
