-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;


entity tx_parse_addr_read_addr is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iEop: in std_logic;

        iValid: in std_logic;
        oRd: out std_logic;
        iSpw_data: in std_logic_vector (8 downto 0);

        oId_valid: out std_logic;
        oId: out std_logic_vector (7 downto 0)
    );
end entity;

architecture v1 of tx_parse_addr_read_addr is

    component reg
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iData: in std_logic_vector;
            oData: out std_logic_vector
        );
    end component;

    component reg_en
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iEn: in std_logic;
            iData: in std_logic_vector;

            oData: out std_logic_vector
        );
    end component;

    signal sState: std_logic;
    signal sState_reg: std_logic;
    signal sState_n: std_logic;

begin

    oRd <= sState_n;
    oId_valid <= sState_reg;

    sState_n <= not sState_reg;

    process (sState_reg, iEop, iValid, iSpw_data)
    begin
        if sState_reg = '0' then
            if iValid = '1' and iSpw_data (8) = '0' then
                sState <= '1';
            else
                sState <= '0';
            end if;
        else
            if iEop = '1' then
                sState <= '0';
            else
                sState <= '1';
            end if;
        end if;
    end process;

    state_reg: reg
        port map (
            iClk => iClk,
            iReset => iReset,

            iData (0) => sState,
            oData (0) => sState_reg
        );

    id_reg: reg_en
        port map (
            iClk => iClk,
            iReset => iReset,

            iEn => sState_n,
            iData => iSpw_data (7 downto 0),

            oData => oId
        );

end v1;
