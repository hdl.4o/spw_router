-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.spw_router_pkg.all;


entity tester is
    generic (
        gReset_active_lvl: std_logic := '0'
    );
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iPorts: in tSpw_halfport_ar;
        oPorts: out tSpw_halfport_ar;

        test_done: out std_logic;
        test_error: out std_logic
    );
end entity;

architecture v1 of tester is

    constant cN: natural := iPorts'length;

    type tPkg is array (natural range <>) of std_logic_vector (8 downto 0);

    function compose_pkg (path: natural) return tPkg is
    begin
        return tPkg'(
            0 => '0' & std_logic_vector (to_unsigned (path+1, 8)),
            1 => '0' & x"55",
            2 => '1' & x"00"
        );
    end function;

    component tester_fifo
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iValid: in std_logic;
            oRd: out std_logic;
            iData: in std_logic_vector (8 downto 0);

            oValid: out std_logic;
            iRd: in std_logic;
            oData: out std_logic_vector (8 downto 0)
        );
    end component;

    signal sValid: std_logic_vector (cN-1 downto 0);
    signal sRd: std_logic_vector (cN-1 downto 0);
    signal sData: tSpw_data_ar (cN-1 downto 0);

    signal sValid_o: std_logic_vector (cN-1 downto 0);
    signal sRd_o: std_logic_vector (cN-1 downto 0);
    signal sData_o: tSpw_data_ar (cN-1 downto 0);

begin

    assign_outputs: for i in cN-1 downto 0 generate

        oPorts(i).valid <= sValid_o (i);
        oPorts(i).data <= sData_o (i);
        oPorts(i).rd <= sRd_o (i);

    end generate;

    process

        procedure send_symbol (n: natural; s: std_logic_vector (8 downto 0)) is
        begin
            wait until iClk'event and iClk = '0';
            sData_o (n) <= s;
            sValid_o (n) <= '1';
            if iPorts(n).rd /= '1' then
                wait until iPorts(n).rd = '1';
            else
                wait until iClk'event and iClk = '0';
            end if;
            --wait_rd: while true loop
            --    wait until iClk'event and iClk = '1';
            --    if iPorts(n).rd = '1' then
            --        exit wait_rd;
            --    end if;
            --end loop;
            sValid_o (n) <= '0';
            wait until iClk'event and iClk = '0';
        end procedure;

        procedure send_pkg (n: natural; pkg: tPkg) is
        begin
            for i in pkg'low to pkg'high loop
                report "sending " & integer'image (i);
                send_symbol (n, pkg (i));
            end loop;
        end procedure;

        procedure check_symbol (n: natural; s: std_logic_vector (8 downto 0)) is
        begin
            wait_valid: while true loop
                wait until iClk'event and iClk = '1';
                if sValid (n) = '1' then
                    exit wait_valid;
                end if;
            end loop;

            assert sData (n) = s
                report "symbol check failed"
                severity failure;

            wait until iClk'event and iClk = '0';
            sRd (n) <= '1';
            wait until iClk'event and iClk = '0';
            sRd (n) <= '0';
        end procedure;

        procedure check_pkg (n: natural) is
        begin
            report "checking 55";
            check_symbol (n, '0' & x"55");
            report "checking eop";
            check_symbol (n, '1' & x"00");
        end procedure;

        procedure test_ports (first: natural; last: natural) is
        begin
            send_pkg (first, compose_pkg (last));
            check_pkg (last);
        end procedure;

    begin
        for i in oPorts'range loop
            sValid_o (i) <= '0';
            sData_o (i) <= (others => '0');
        end loop;
        sRd <= (sRd'range => '0');
        test_done <= '0';
        test_error <= '0';
        if iReset /= gReset_active_lvl then
            wait until iReset = gReset_active_lvl;
        end if;
        wait until iReset = not gReset_active_lvl;

        for i in iPorts'range loop
            for j in iPorts'range loop
                report "testing ports: " & integer'image (i) & " to "
                    & integer'image (j);
                test_ports (i, j);
            end loop;
        end loop;

        wait for 100 ns;
        test_done <= '1';
        wait;
    end process;

    fifo_ar: for i in cN-1 downto 0 generate

        fifo: tester_fifo
            port map (
                iClk => iClk,
                iReset => iReset,

                iValid => iPorts(i).valid,
                oRd => sRd_o (i),
                iData => iPorts(i).data,

                oValid => sValid(i),
                iRd => sRd(i),
                oData => sData(i)
            );

    end generate;

end v1;
