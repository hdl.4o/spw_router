-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;


entity tester_fifo is
    generic (
        gReset_active_lvl: std_logic := '0'
    );
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iValid: in std_logic;
        oRd: out std_logic;
        iData: in std_logic_vector (8 downto 0);

        oValid: out std_logic;
        iRd: in std_logic;
        oData: out std_logic_vector (8 downto 0)
    );
end entity;

architecture v1 of tester_fifo is

    constant cN: natural := 256;

    type tRam is array (cN-1 downto 0) of std_logic_vector (8 downto 0);

begin

    process (iClk, iReset)
        variable n: natural;
        variable ram: tRam;
        variable rd_addr: natural;
        variable wr_addr: natural;
    begin
        if iReset = gReset_active_lvl then
            n := 0;
            oRd <= '0';
            oValid <= '0';
            oData <= (oData'range => '0');
        else
            if iClk'event and iClk = '1' then
                oRd <= '0';

                -- rd
                if iRd = '1' then
                    if n > 0 then
                        n := n -1;
                        rd_addr := rd_addr +1;
                    end if;
                    report "rd. n = " & integer'image (n);
                end if;

                -- wr
                if iValid = '1' then
                    if n < 255 then
                        n := n +1;
                        oRd <= '1';
                        ram (wr_addr) := iData;
                        wr_addr := wr_addr +1;
                    end if;
                    report "wr. n = " & integer'image (n);
                end if;

                oData <= ram (rd_addr);
                if n > 0 then
                    oValid <= '1';
                else
                    oValid <= '0';
                end if;
            end if;
        end if;
    end process;

end v1;
