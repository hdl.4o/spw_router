-- Copyright (c) 2018, 4o All rights reserved. Redistribution and use in source
-- and binary forms, with or without modification, are permitted provided that
-- the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- The views and conclusions contained in the software and documentation are
-- those of the authors and should not be interpreted as representing official
-- policies, either expressed or implied, of the <project name> project.


library ieee;
use ieee.std_logic_1164.all;


entity testbench is
    generic (
        gReset_active_lvl: std_logic := '0'
    );
end entity;

architecture v1 of testbench is

    component test_wrap
        generic (
            gN: natural
        );
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            test_done: out std_logic;
            test_error: out std_logic
        );
    end component;

    signal iClk: std_logic;
    signal iReset: std_logic;
    signal test_done: std_logic;
    signal test_error: std_logic;

begin

    assert test_done /= '1'
        report "test done :)"
        severity failure;

    assert test_error /= '1'
        report "test_error :("
        severity failure;

    process
    begin
        iClk <= '0';
        wait for 5 ns;
        iClk <= '1';
        wait for 5 ns;
    end process;

    process
    begin
        iReset <= gReset_active_lvl;
        wait for 1002 ns;
        iReset <= not gReset_active_lvl;
        wait;
    end process;

    uut_wrap: test_wrap
        generic map (
            gN => 2
        )
        port map (
            iClk => iClk,
            iReset => iReset,

            test_done => test_done,
            test_error => test_error
        );

end v1;
